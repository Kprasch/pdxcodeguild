# Executing a .py File

Executing a file with the .py extension is straight-forward.

```cmd
$ python llamas.py
```

There are some noteworthy "switches":

```cmd
$python -i llamas.py
```

will run your python file, and then drop the user into the interactive interpreter session at the end of the file.

```cmd
$ python -c 'print("lizard")'
```

Will execute a one liner

```cmd
$ python -m <some_module_name> llamas.py

$ python -m doctest llamas.py
```

Finally, the -m switch allows the importing of a module.

### Python 2 vs. 3 ###

Be aware which version of python you are running when calling python files from the command line.

```cmd
$ python llamas.py
$ python3 llamas.py
```

```cmd
python -V
```
