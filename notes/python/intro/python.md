## Python ##

Generally if you ask what python is you might hear them say things like:
  - General Purpose
  - High Level
  - Object Oriented
  - Interpreted
  - Dynamically Typed
  - Strongly Typed
  - White Space Delimited

Let's break this down.

####General Purpose###

Many languages exhibit strength at some specific tasks.  While python is well-suited for object oriented programming, it is wholesomely practical for general computing needs.

####High Level

To define the bounds of the 'high level' / 'low level' paradigm, imagine the lowest level language possible as binary and the highest level possible as python.  Through time, languages are built upon languages, *abstracting* away from the bare metal to make complex tasks easier.

High-level languages are optimized for human consumption.
Low-level languages are better for machines.
There are distinct advantages to both.

Python has embraced this concept, and has a unique focus on readability.

####Interpreted

Programming languages are often referred to as 'interpreted' or 'compiled'.

In reality *languages* are not one or the other, rather, *language implementations* are interpreted or compiled.

The truth about python can be blurry because of the python bytecode compiler but there are some clear signs of a truly compiled language.

C code for example, typically compiles to assembler code. It is transformed into *litterally* another language before it is executed.

Python has a built in bytecode compiler for executing source code in whats known as the 'python virtual machine', but interaction with python is done with an 'interpreter'.


####Dynamicaly Typed

Dynamic typing means that variables (and their types) are interpreted at runtime.

Some languages (like Java and C++) are staticly typed. This means that the types of variables must be known at compile time.  One evident manifestation of a language with static typing is an explicit declaration of the variables type before its assignment.

For example in C++:

```c++
int my_var;

my_var = 7;
```

####White Space Delimited

As part of the effort to design python for humans, whitespace is actually interpreted at runtime, thus misuse of whitespace can result in IndentationError.



## Additional Resources ##

[Python Executive Summary](https://www.python.org/doc/essays/blurb/)

[Wikipedia](https://en.wikipedia.org/wiki/Python_(programming_language))
