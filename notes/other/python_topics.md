# PDX Code Guild - Python Full Stack Evening Class

# July 12th to November 4th Evening Class

## Intro

#### Administration ####
1. [Goals](/intro/course-learning-objectives.md)
1. [Expectations](/intro/course-expectations.md)
1. [Schedule](/intro/high-level-schedule.md)
1. [Day-to-Day](/intro/high-level-process.md)
1. [Logistics](/intro/course-logistics.md)
1. [Certificate Requirements](/intro/course-requirements.md)
1. [Portfolio](/intro/course-portfolio.md)
1. [Conduct](/intro/course-conduct.md)
1. [Working with Others](/intro/group-work.md)
1. [Post-Course](/intro/course-post.md)
1. [Computer Set Up](/intro/software.md)
1. [What I Tell All New Programmers](https://josephg.com/blog/what-i-tell-all-new-programmers/)
1. Impostor Syndrome

#### Command Line
1. [Filesystem](/intro/filesystem.md)
1. [Command Line Interface Tour](/intro/command-line-tour.md)
1. [Lab: CLI](/labs/cli.md)

## Python

### Intro ###
  1. [Glossary](https://docs.python.org/3/glossary.html#term-decorator)
  1. [What is Python?](/python/intro/python.md)
  1. [Stored Program Computer](/intro/stored-program-computer.md)

#### The Python Interactive Interpreter
1. [Interpreter Exploration](/intro/shell-games.md)
1. [Programming Philosophy](/python/intro/programming-philosophy.md)

### Authoring Python Files ###
1. [Executing Python Files](/python/intro/executing_files.md)
1. Command Line Editors
1. [Atom Editor](/intro/atom.md)
1. [Path of Execution](/python/authoring_files/py-program-flow.md)
1. [Lab: star_wars.py](/labs/star_wars.py)

1. [Python Style](/python/authoring_files/py-style.md)
1. [Comments](/python/authoring_files/py-comments.md)
1. [Modules](/python/authoring_files/py-modules-create.md)
1. [Module Docstrings](/python/authoring_files/py-docstrings-module.md)

### Types and Variables ###
1. [Values and Types](/python/intro/values-and-types.md)
1. Equality Vs. Identity
1. Mutability
1. Number and String Intro
1. [Operators and Expressions](/python/types_and_variables/py-operators-expressions.md)
1. [Basic Math](/python/types_and_variables/py-math.md)
1. [Variables](/python/types_and_variables/py-variables.md)
1. [Basic Variable Naming](/python/types_and_variables/naming-variables-basic.md)
1. [Basic Type Casting](/python/functions/py-casting.md)
1. [Comparison Operators](/python/flow_control/py-operators-comparison.md)
1. [Advanced Variable Assignment](/python/types_and_variables/py-assignment-adv.md)
1. [Terminal IO]
1. [Lab: Name Banner](/labs/name_banner.md)
1. [Lab: Greeting](/labs/greeting.md)
1. [Lab: Madlib](/practice/madlib.md)
1. [Lab: Wall Painting](/practice/wall-painting.md)

### Using Functions ###
1. [Interfaces and Scope](/python/functions/scope-interfaces.md)
1. [Function Calling](/python/functions/function-calling.md)
1. [BuiltIns](https://docs.python.org/3/library/functions.html)
1. [Function Signatures](/python/functions/function-signatures.md)
1. [Standard Library Docs](/python/functions/py-standard-library-docs.md)
1. [Cheat Sheet](/python/functions/mementopython3-english.pdf)
1. [Function Calling](/python/functions/py-functions-calling-basic.md)
1. [Calling Class Methods](/python/functions/py-type-methods-calling.md)
1. String Manipulation and Indexing
1. [Basic String Methods](/python/functions/py-string-methods-basic.md)
1. [String Formatting](/python//functions/py-string-format.md)

1. [Lab: Pig Latin](/practice/pig-latin.md)
1. [Lab: Volume Converter](/labs/volume-converter.md)
1. [Lab: Programming Case Conversion](/practice/case.md)
1. [Lab: Phone Number](/labs/phone-number.md)

### Defining Functions ###
1. Importing & Anti-Gravity
1. [Interfaces and Scope](/python/functions/scope-interfaces.md)
1. Garbage Collection
1. [Writing Functions](/python/py-functions-defining.md)
1. [Lab: knights.py](/labs/knights.py)
1. [Lab: strings.py](/labs/strings.py)
1. [Lab: palindrome detector](/labs/palindrome.py)
--------------------------------------------------------------------------------
1. Common String Operations
1. [Lab: join.py](/labs/format_and_cast.py)
1. [Lab: extract_domain.py](/labs/extract_header.py)
1. [Branching and Blocks](/python/py-branching-blocks.md)
1. [Boolean Operators](/python/py-operators-boolean.md)
1. [Comparison Operators](/python/flow_control/py-operators-comparison.md)
1. [Lab: Bool Game](/labs/bool-game.md)
--------------------------------------------------------------------------------
1. [Lab: String Operations]
1. [Lab: Change Return](/labs/change_return.py)
1. [Lab: hammer.py](/labs/hammer.py)
1. [Lab: 21 Advice](/practice/21.md)
1. [Lab: Written-Out Numbers](/labs/written-numbers.py)

### List Type ###
1. Containers
1. [Lists](/python/containers/py-lists.md)
1. Basic Iteration
1. [For Loops](/python/containers/py-for-loops.md)
1. [Slicing](/python/containers/py-operators-sequence.md)
1. [Lab: list_intro.py](/labs/list_intro.py)
1. [Lab: list_patterns.py]
1. [Lab: list_practice.py]
1. [Lab: list_practice2.py]
1. [Lab: nest.py]

### More on Functions ###
1. [Optional / Default Arguments](/python/functions/py-functions-calling-optional.md)
1. Arbitrary Parameters Intro
1. [Lab: Args and Kwargs](Lab: arb_args.py)
1. [Function Naming](/python/naming-functions.md)
1. [Function Docstrings](/python/py-docstrings-function.md)
1. Importing Modules Review
1. Recursive Function Calls
1. [Lab: Magic Ball](/labs/magic_ball.md)
1. [Continuations](/python/problem_solving/py-continuations.md)
1. [Basic Debugging](/python/problem_solving/debugging-basic.md)
1. [Basic Problem Solving](/python/problem_solving/problem-solving-basic.md)
-----------------------------------------------------------------------------
### Version Control ###
1. [Version Control](/version_contorl/version-control.md)
1. [Basic Git](/version_contorl/git-basic.md)
1. [Git Ignore](/version_contorl/git-ignore.md)
1. [Git Remotes and GitHub](/version_control/git-github.md)
1. [Git Workflow](/version_contorl/git-workflow-basic.md)

### Containers ###
1. List Sorting
1. [Lab: list_patterns.py](/labs/list_patterns.py)
1. [Lab: Last Join](/practice/last-join.md)
1. [Lab: I-E Spelling](/labs/i_before_e.py)
1. [Lab: Shrink](/labs/shrink.py)
1. [Lab: ROT13](/labs/rot13.md)
1. [Dictionaries](/python/containers/py-dicts.md)
1. [Lab: Word Histogram](/labs/quantify_words.py)
1. [Choosing Data Structures](/python/problem_solving/problem-solving-data-structures.md)

### Exceptions ###
1. Basic Error Handling
1. Raising Exceptions
1. The Most Diabolical Python Anti-Pattern
1. EAFP vs. LBYL
