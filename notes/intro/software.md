# Software

If you don’t already, make sure you have all of the following installed on your system:

- [git](https://git-scm.com/downloads)
- [Python 3](https://www.python.org/downloads/)
- [Atom](https://atom.io/) and/or [PyCharm](https://account.jetbrains.com/a/aoszpv)
- [Slack](https://slack.com/downloads)
- [Google Chrome](https://www.google.com/chrome/browser/desktop/)

------

### Python 3

For the sake of convenience, you may wish to set up your system so that typing the command `python` at the command line launches Python 3, rather than your system default (usually Python 2.7). Here are [directions for how to do that on OS X](http://stackoverflow.com/a/18425592).

Here is how I have my `~/.bash_aliases` file set up:

    alias python='python3'
    alias python2='\python'

By using the above, I can type `python` to default to Python 3, but still launch Python 2 by typing `python2`. 99% of the time in class we will be using Python 3.

------

### Atom

Open Atom and install the following packages:

- python-tools
- python-indent
- python-debugger
- autocomplete-python
- language-python
- python-autopep8
- linter
- linter-python-pep8
- linter-sass-lint
- linter-eslint

------

### Slack

Our Slack team is [portlandcodeguild.slack.com](https://portlandcodeguild.slack.com). If you don’t already have an invite, let me know. Our class channel is [#hedges-summer-2016](https://portlandcodeguild.slack.com/archives/hedges-summer-2016).

------

### GitHub

We will be using [GitHub](https://github.com) to collaborate. If you don’t already have an account, create one now then post your username in the [class Slack channel](https://portlandcodeguild.slack.com/archives/hedges-summer-2016).

The GitHub repo for our class is at [github.com/segdeha/pdxcodeguild](https://github.com/segdeha/pdxcodeguild)

------

### Trello

We will be using [Trello](https://trello.com) to manage assignments. If you don’t already have an account, create one now then post your username in the [class Slack channel](https://portlandcodeguild.slack.com/archives/hedges-summer-2016).

------

### Stack Overflow

I recommend creating an account on [Stack Overflow](http://stackoverflow.com/), if you don’t already have one. The site will quickly become your best friend when you run into problems or have programming questions.


# Python

The programming language we'll be learning in this course is [Python](https://www.python.org).

It is an interpreted language.
You save your source in a `.py` file and a special program called the **interpreter** knows how to run that file.

## Install

### OS X

First, install [Homebrew](http://brew.sh).
It's a package manager that allows you to install other command line tools very easily.
We'll use it a few times later in the class as well.

Then, install Python 3 and setup the Python command.

```bash
brew install python3
echo "alias python='python3'" >> .bashrc
echo "alias python2='\python'" >> .bashrc
echo "alias pip='pip3'" >> .bashrc
echo "alias pip2='\pip'" >> .bashrc
alias python='python3'
alias python2='\python'
alias pip='pip3'
alias pip2='\pip'
```

### Windows 7 and 10


![](py.images/0000.png)
[Download](https://www.python.org/downloads/) the Python 3 installer and run it.

![](py.images/0001.png)
If you are not sure then please ask which executable you should download. Most people will be needing `x86-64 executable installer` because they have 64-bit processors and admin priveleges on their machine.

![](py.images/0002.png)
![](py.images/0003.png)
Be sure to select, **Add Python 3.5 to PATH**

![](py.images/0004.png)
![](py.images/0005.png)

![](py.images/0006.png)
To verify your install, please launch powershell.

![](py.images/0007.png)
![](py.images/0008.png)


![](py.images/0009.png)
**pip** is the python package manager. This will become increasingly important later. To update **pip** please run `pip install --upgrade pip`

![](py.images/0012.png)
**ipython** is a very helpful python-shell that i will sugest use for understanding code.

## Running

Once you've installed Python, you should be able to run Python source files.

```bash
~ $ python -V
Python 3.5.1
```

Also check that you can run PIP.

```bash
~ $ pip -V
pip 8.1.2 from /usr/local/lib/python3.5/site-packages (python 3.5)
```
