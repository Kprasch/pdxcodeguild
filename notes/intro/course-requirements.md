## Basis for Final Grade

Grading is on a Pass With Distinction/Pass/Fail scale. Students are graded on each individual section based on specific in-class exercises and/or projects and tech challenges given at the end of each unit.

**In-class exercises:** Students edit and debug each exercise or project until it passes. All exercises and projects submitted for grading must pass for the student to pass the course. To receive a grade of Pass with Distinction, student must do more than the minimum required on exercises, successfully using additional skills outside the lesson, for exercises and work on their project.

**Pair Programming:** A portion of your grade in this section will be based on your interpersonal skills while pair-programming. You must pass pair-programming to pass the class.

## Course Expectations

### Timely work

Students work on projects in class with the support of the instructor, other students, documentation and online resources. Students work on projects until they pass. Students are expected to keep up with the rest of the class. Students unable to keep up with the class will be asked to have a meeting with the instructor and director to develop a plan to get student caught up. If student fails to get back on track within two weeks, student may be asked to take a leave of absence until the student’s situation is changed sufficiently to allow student to keep up with the class.

### Attendance

PDX Code Guild maintains attendance records for each student. Students are expected to be on time and attend all scheduled class times. The school requires ninety percent (90%) completion of class hours in order to receive a certificate of completion from the course. If in any 14-day period your attendance is less than 90%, you will be notified and placed on probation for a period of 14 days. If you meet the attendance requirement in the subsequent 14 days, you will be removed from probation. If you fail to correct your attendance problem, you will be dismissed from the school.

If dismissed from the school, you will be eligible for re-admittance without filling out a new application after a minimum period of 60 days. You may be required to provide proof that the problem that caused your chronic absenteeism has been resolved.

### Conduct

Students are expected to comply with the PDX Code Guild Code of Conduct. Students are given a copy of the PDX Code Guild code of conduct and a copy of the school catalog containing the Code of Conduct and Policy on Code of Conduct Infringement upon registration. Please refer to your copy of the code of conduct and Policy on Code of Conduct Infringement sections of the school catalog for more information.

### Plagiarism

All work submitted must be the student’s own work. It is acceptable and expected that students will use online and print resources and work with classmates to complete assignments. It’s normal in programming to use bits of code that someone else has written. Be careful when doing this; you must make sure that you understand what each line of code does. Give credit when you work with others or use parts of existing code.

For example:

    # based on: http://stackoverflow.com/a/931095/11577
    reversedStr = 'Eva, can I see bees in a cave?'[::-1]

It is unacceptable to copy someone else’s work in its entirety and submit it as your own work.


## Materials and software used in this course

Students are required to bring and use their own laptop for the course. Any operating system is fine, but the machine should be new enough to run several applications at once without slowing down considerably.
