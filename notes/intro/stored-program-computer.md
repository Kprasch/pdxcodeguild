# Fixed Program Computer
Your calculator is a computer ( a circuit ) that has been designed to perform arithmetic.

For example, your calculator can output the sum of 1 + 1 just fine, but if you want to use it as a word processor, well, good luck.

Fixed program computers designed, not programmed.

# Stored Program Computer
Now, imagine a circuit, that when given a schematic of another circuit, reconfigures itself to behave as desired, following the schematic.

Your computer uses the **stored program model**.
This means the instructions for each program are stored as _files_ on the hard drive.

When you are writing Python code, you create a `.py` file that stores all the instructions.
You then tell the computer to later **execute** that code.
It will follow the instructions, but this time on whatever _new input_ it gets from the user that time.

When a computer does work, it is following a saved recipe.
That recipe is saved in a **file**.
That file contains the **source code** for the program.

In this section of the course, the source code will be written in Python.
It is a **programming language**.
It is very specific textual instructions for the computer.

The whole goal of this class is to generate carefully crafted files that know how to solve specific problems.
