# Practice: CLI

First, make a `codeguild` directory in your home directory.
This will be your [course portfolio](/intro/course-portfolio.md).

Then, as practice, make the following directory and file structure inside of it:

```
~/codeguild/
    parent/
        child/
            toys.txt
        taxes.txt
    hello.py
```

Fill `toys.txt` with the text `Mr. Potato Head`, `taxes.txt` with the text `Are time consuming`, and `hello.py` with the text `print('Hello! This program ran!')` using Atom.
